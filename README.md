# okteto-jvm
`docker pull abitofhelp/okteto-jvm`

This project contains a Docker image for JVM development with Okteto.
It contains the following key technologies:
* AdoptOpenJdk
* Azure CLI
* Gradle
* Kotlin
* Okteto CLI
* Ubuntu